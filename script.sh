#!/bin/bash

# Program-Name:     Discord Chat Exporter to local Filesystem
# Filename:         script.sh
# Description:      This script inputs the export (html) of Discord Chat Exporter and 
#                   converts it to a website that includes all dependencies (attachements and avatar images)
# Author:           Yanik Ammann
# Last Changed:     18.02.2020 by Yanik Ammann
# ToDo:             -
# Dependencied:     bash, curl, sed, (optipng, jpegoptim)

# Versions (version-number, date, author, description):
# v0.0, 18.02.2020, Yanik Ammann, sketching
# v0.1, 18.02.2020, Yanik Ammann, starting developement
# v0.2, 20.02.2020, Yanik Ammann, finalized some code

optimizePNG=true
optimizeJPG=true

# check if dependencies are installed
if ! [ -x "$(command -v curl)" ]; then
    echo "Error: curl is not installed"
    exit 1
fi
if ! [ -x "$(command -v sed)" ]; then
    echo "Error: sed is not installed"
    exit 1
fi

if ! [ -x "$(command -v optipng)" ]; then
    echo "Warning: optipng is not installed, installation will continue without it"
    optimizePNG=false
fi
if ! [ -x "$(command -v jpegoptim)" ]; then
    echo "Warning: jpegoptim is not installed, installation will continue without it"
    optimizeJPG=false
fi

# check if output is not empty //////////////////////////// UPDATE ////////////////
mkdir -p export
if [ "$(ls -A export)" ]; then
    echo "Warning: Export directory is not empty"
    read -p "Do you want to delete the folder contents now? [Y,n] " -i Y ifDeleteFolder
    if [[ $ifDeleteFolder == 'Y' ]] || [[ $ifDeleteFolder == 'y' ]] || [[ $ifDeleteFolder == '' ]]; then
        echo "Deleting folder contents..."
        rm -r -v export/
        echo "Done"
    fi
fi

# deleting last run
echo "deleting last run..."
rm -f import/*.txt

# create folder structure
echo "creating folder structure..."
mkdir -p import
mkdir -p export

# user needs to copy files
read -p "Now copy the input .html files into the input folder [OK] "

# rename files
for f in import/*.html
do
    read -p "What name do you want to rename $f to? " ifFileName
    mv "$f" import/"$ifFileName".html
done

# convert images to txt
for f in import/*.html
do
    echo "Processing file $f..."
    cat "$f" | grep img | grep cdn.discordapp.com | grep avatars >> import/avatars.txt
    cat "$f" | grep img | grep cdn.discordapp.com | grep attachments >> import/attachments.txt
done

# only keep links
for f in import/*.txt
do
    echo "Processing links in $f..."
    sed -i -e 's/<.*src="//g' "$f"
    sed -i -e 's/" \/>//g' "$f"
    sed -i -e 's/ //g' "$f"
done

# remove dublicates
for f in import/*.txt
do
    echo "Removing dublicate lines from $f..."
    sort "$f" | uniq | sed 's/\r$//g' > "$f".txt
done

# move .txt.txt to .txt
for f in import/*.txt.txt
do
    mv "$f" "${f/%????/}"
done

# export
read -p "Do you want to use folders (with index.html) instead of direct HTML files? [Y,n] " -i Y ifFolder
if [[ $ifFolder == 'Y' ]] || [[ $ifFolder == 'y' ]] || [[ $ifFolder == '' ]]; then
    echo "Using folders"

    # create export folder structure and copy files
    for f in import/*.html
    do
        echo "Creating folder $(echo export/$(echo "${f/%?????/}" | cut -c 8-))"
        mkdir $(echo export/$(echo "${f/%?????/}" | cut -c 8-))
        cp "$f" $(echo export/$(echo "${f/%?????/}" | cut -c 8-)/index.html)
    done

    # fixing urls
    for f in export/*/*.html
    do
        echo "fixing urls in $f"
        sed -i -e 's/https:\/\/cdn.discordapp.com/../g' "$f"
    done
else
    echo "Using files"

    # copy files
    for f in import/*.html
    do
        echo "Copying file $f to $(echo "export/"$(echo "$f"| cut -c 8-))"
        cp "$f" $(echo "export/"$(echo "$f"| cut -c 8-))
    done

    # fixing urls
    for f in export/*.html
    do
        echo "fixing urls in $f"
        sed -i -e 's/https:\/\/cdn.discordapp.com\///g' "$f"
    done
fi

# ask for download script parameters
echo
read -p "Do you want to generate download script instead of downloading them now? [y,N] " -i N ifFolder
if [[ $ifFolder == 'Y' ]] || [[ $ifFolder == 'y' ]]; then
    echo "Generating download script... (this might take a while)"
    
    # download-Avatars script
    echo "#/bin/bash" > export/downloadAvatars.sh
    while IFS= read -r line
    do
        mkdir -p $(echo "export/"$(echo "$line" | sed 's/https:\/\/cdn.discordapp.com\///g' | sed 's%/[^/]*$%/%'))
        echo curl -o $(echo ""$(echo "$line" | sed 's/https:\/\/cdn.discordapp.com\///g')) "$line"  >> export/downloadAvatars.sh
    done < import/avatars.txt

    # dowload-Attachments script
    echo "#/bin/bash" > export/downloadAttachments.sh
    while IFS= read -r line
    do
        mkdir -p $(echo "export/"$(echo "$line" | sed 's/https:\/\/cdn.discordapp.com\///g' | sed 's%/[^/]*$%/%'))
        curl -o $(echo ""$(echo "$line" | sed 's/https:\/\/cdn.discordapp.com\///g')) "$line"  >> export/downloadAttachments.sh
    done < import/attachments.txt
    echo
    echo "In order to start the download process, execute the generated scripts"
else
    # download Avatars
    echo "Starting download of avatars..."
    while IFS= read -r line
    do
        path=$(echo $line | sed 's/https:\/\/cdn.discordapp.com\///g')
        mkdir -p export/$(echo $path | sed 's%/[^/]*$%/%')
        curl -o "export/""$path" "$line"
    done < import/avatars.txt

    # download Attachments
    echo "Starting download of Attachments..."
    while IFS= read -r line
    do
        path=$(echo $line | sed 's/https:\/\/cdn.discordapp.com\///g')
        mkdir -p export/$(echo $path | sed 's%/[^/]*$%/%')
        curl -o "export/""$path" "$line"
    done < import/attachments.txt
    echo
fi

# optimize PNGs
if [[ $optimizePNG == true ]]; then
    find export -type f -name *.png -exec optipng {} \;
fi

# optimize JPGs
if [[ $optimizeJPG == true ]]; then
    find export -type f -name *.png -exec jpegoptim {} \;
fi
